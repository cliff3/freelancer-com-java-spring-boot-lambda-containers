# References
- https://github.com/awslabs/aws-serverless-java-container
- https://www.digitalocean.com/community/tutorials/how-to-install-java-with-apt-on-ubuntu-20-04
- https://docs.aws.amazon.com/lambda/latest/dg/lambda-runtimes.html

# Installation
- install java 11 (AWS lambda java runtime will be java 11 for this project)
- install docker
- install aws
- install sam

# Deployment
- sam build
- sam deploy -g
